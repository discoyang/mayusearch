CXX 		:=g++
TARGET 		:=./bin/server

CCS 		:=$(wildcard ./src/*.cc)
OBJS 		:=$(CCS:%.cc=%.o)

# 要包含的库
LIBS 		:=pthread

# 编译器选项
CXXFLAGS 	:=-Wall
# 链接器选项
LDFLAGS 	:=$(LIBS:%=-l%)

# 以debug（默认）/release模式编译
# 如果想转换成release模式可以在命令行使用'make M=release'
M 			:=debug

ifeq ($(M),release)
# release模式编译器选项
# 启用O2级别优化
CXXFLAGS 	+=-O2
# 忽略未使用参数
# CXXFLAGS 	+=-Wno-unused-parameter
# 启用异常处理
# CXXFLAGS 	+=-fexceptions
else
# debug模式编译器选项
# 保留调试信息
CXXFLAGS 	+=-g3 -ggdb
# 条件编译
CXXFLAGS 	+=-DDEBUG
# 产生DWARF version2格式的调试信息
# CXXFLAGS 	+=-gbwarf-2
# 栈溢出时报错
# CXXFLAGS 	+=-fstack-protector-all
# 在汇编代码中保留栈帧操作
# CXXFLAGS 	+=--fno-omit-frame-pointer
endif

$(TARGET):$(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS)

%.o:%.c
	$(CXX) -c $< -o $@ $(CXXFLAGS)

.PHONY:clean rebuild

# 清理所有目标文件和可执行文件
clean:
	$(RM) $(OBJS) $(TARGET) 

# 重建可执行文件
rebuild:clean $(TARGET)
