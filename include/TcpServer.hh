#ifndef _TCPSERVER_HH
#define _TCPSERVER_HH

#include "Acceptor.hh"
#include "EventLoop.hh"

namespace mayu
{
class TcpServer
{
public:
    TcpServer(const std::string &__ip, unsigned short __port);

    ~TcpServer();

    // 开启服务器
    void start();

    // 关闭服务器
    void stop();

    // 设置所有回调函数
    void set_all_callback(TcpConnectionCallback &&__on_connection,
                          TcpConnectionCallback &&__on_message,
                          TcpConnectionCallback &&__on_close);

private:
    Acceptor _acceptor;

    EventLoop _loop;

}; /* end of class TcpServer */

} /* end of namespace mayu */

#endif /* _TCPSERVER_HH */
