/* Socket负责创建套接字
 */

#ifndef _SOCKET_HH
#define _SOCKET_HH

#include "Noncopyable.hh"

namespace mayu
{
class Socket
: public Noncopyable
{
public:
    Socket();

    explicit Socket(int __fd);

    ~Socket();

    int get_fd() const;

    // 关闭写端
    void shutdown_write() const;
    
    // 把套接字属性设置为非阻塞
    void set_nonblock() const;

private:
    int _fd;

}; /* end of class Socket */

} /* end of namespace mayu */

#endif /* _SOCKET_HH */ 
