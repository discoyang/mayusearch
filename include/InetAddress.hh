/* InetAddress负责存储IP地址和端口号
 */

#ifndef _INETADDRESS_HH
#define _INETADDRESS_HH

#include <arpa/inet.h>
#include <string>

namespace mayu
{
class InetAddress
{
public:
    InetAddress(const std::string &__ip, unsigned short __port);

    InetAddress(const struct sockaddr_in &__addr);
    
    ~InetAddress();

    std::string get_ip() const;

    unsigned short get_port() const;

    const struct sockaddr_in *get_sockaddr_ptr() const;

private:
    struct sockaddr_in _addr;

}; /* end of class InetAddress */

} /* end of namespace mayu */

#endif /* _INETADDRESS_HH */
