/* Configuration管理配置内容
 * 已经实现单例模式
 * 不保证线程安全
 */

#ifndef _CONFIGURATION_HH
#define _CONFIGURATION_HH

#include <map>
#include <set>
#include <string>

namespace mayu
{
struct ConfigArgs
{
    //网络
    const std::string IP_ADDRESS = "IP_ADDRESS";
    const std::string PORT = "PORT";

    //jieba库参数
    const std::string DICT_PATH = "DICT_PATH";
    const std::string HMM_PATH = "HMM_PATH";
    const std::string USER_DICT_PATH = "USER_DICT_PATH";
    const std::string IDF_PATH = "IDF_PATH";
    const std::string STOP_WORD_PATH = "STOP_WORD_PATH";

    //词典路径
    const std::string CN_DICT_PATH = "CN_DICT_PATH";
    const std::string EN_DICT_PATH = "EN_DICT_PATH";

    //索引表路径
    const std::string CN_INDEX_PATH = "CN_INDEX_PATH";
    const std::string EN_INDEX_PATH = "EN_INDEX_PATH";

    //LRUCache的capacity
    const std::string LRUCACHE_CAPACITY = "LRUCACHE_CAPACITY";

    //线程数量  
    const std::string THREAD_NUM = "THREAD_NUM";
    
    //任务队列的长度
    const std::string TASKQUE_SIZE = "TASKQUE_SIZE";
    
    //cache文件存储位置
    const std::string KEY_CACHE_STORE_PATH = "KEY_CACHE_STORE_PATH";
    const std::string WEBPAGE_CACHE_STORE_PATH = "WEBPAGE_CACHE_STORE_PATH";
    
    //候选词数量
    const std::string CANDIDIDATE_NUMBER = "CANDIDIDATE_NUMBER";

    //网页模块路径
    const std::string INVERT_INDEX = "INVERT_INDEX";
    const std::string OFFSET = "OFFSET";
    const std::string RIPEPAGE = "RIPEPAGE";

}; /* end of struct ConfigArgs */


// 配置内容类型
using ConfigMapType = std::map<std::string, std::string>;
// 停用词词集
using StopWordSetType = std::set<std::string>;

class Configuration
{
public:
    static Configuration *get_instance();

    static void destroy();

    // 通过配置文件路径初始化配置内容
    void init(const std::string &__filepath);

    // 获取存放配置文件内容的map
    ConfigMapType &get_config_map();

    // 获取停用词词集
    StopWordSetType &get_stop_word_set();

    Configuration(const Configuration &__rhs) = delete;
    Configuration(Configuration &&__rhs) = delete;

    Configuration &operator=(const Configuration &__rhs) = delete;
    Configuration &operator=(Configuration &&__rhs) = delete;

    ConfigArgs map_key;

private:
    Configuration();

    /* Configuration(const std::string &__filepath); */

    ~Configuration();

    void _init_stop_word_set();


    static Configuration *_instance_ptr;

    // 配置文件路径
    std::string _filepath;

    // 配置文件内容
    ConfigMapType _config_map;

    // 停用词词集
    StopWordSetType _stop_word_set;

}; /* end of class Configuration */

} /* end of namespace mayu */

#endif /* _CONFIGURATION_HH */
