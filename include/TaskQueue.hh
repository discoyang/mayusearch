/* TaskQueue管理任务队列
 */

#ifndef _TASKQUEUE_HH
#define _TASKQUEUE_HH

#include <functional>
#include <queue>

#include "Condition.hh"
#include "MutexLock.hh"

namespace mayu
{
// 任务类型
using Task = std::function<void()>;

class TaskQueue
{
public:
    TaskQueue(size_t capacity);

    ~TaskQueue();

    // 队列空返回1，否则返回0
    bool is_empty() const;

    // 队列满返回1，否则返回0
    bool is_full() const;

    void push(Task &&__task);

    Task pop();

    // 唤醒所有阻塞线程
    void stop();

private:
    size_t _capacity;

    std::queue<Task> _que;

    MutexLock _mutex;

    // 当队列满时等待此条件变量
    Condition _not_full;

    // 当队列空时等待此条件变量
    Condition _not_empty;

    // 标志位 1表示运行 0表示退出
    bool _is_running;





}; /* end of class TaskQueue */

} /* end of namespace mayu */

#endif /* _TASKQUEUE_HH */
