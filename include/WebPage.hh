/* WebPage负责存储每一个网页对象
 * 
 * 拥有两个友元函数
 * 分别负责判断网页是否相等（TopK算法或simhash算法）
 * 以及对网页按照网页id进行排序
 */

#ifndef _WEBPAGE_HH
#define _WEBPAGE_HH

#include <map>
#include <string>
#include <vector>

#include "Configuration.hh"

namespace mayu
{
// 配置内容类型
using ConfigMapType = std::map<std::string, std::string>;
// 停用词词集
using StopWordSetType = std::set<std::string>;

class WebPage
{
public:
    WebPage(std::string &__doc, Configuration &__config,
            WordSegmentation &__jieba); 

    ~WebPage();

    // 获取文档的docid
    int get_doc_id();

    // 获取文档的标题
    std::string get_title();

    // 获取文档的摘要
    std::string summary(const std::vector<std::string> &__query_words);

    // 获取文档的词频统计map
    std::map<std::string, int> &get_word_map();

    // 对格式化文档进行处理
    void process_doc(const std::string &__doc, Configuration &__config,
                     WordSegmentation &__jieba);

    // 求取文档的topk词集
    void calc_topk(std::vector<std::string> &__words_vec, int k,
                   StopWordSetType &__stop_word_set);

    // 判断两篇文档是否相等
    friend bool operator==(const WebPage &__lhs, const WebPage &__rhs);

    // 对文档按_doc_id进行排序
    friend bool operator<(const WebPage &__lhs, const WebPage &__rhs);


private:
    constexpr static int TOPK_NUMBER = 20;

    // 文档id
    int _doc_id;

    // 文档标题
    std::string _doc_title;

    // 文档URL
    std::string _doc_url;

    // 文档内容
    std::string _doc_content;

    // 文档摘要，需自动生成，不是固定的
    std::string _doc_summary;

    // 词频最高的TOPK_NUMBER个词
    std::vector<std::string> _top_words;

    // 保存每篇文档的所有词语和词频，不包括停用词
    std::map<std::string, int> _words_map;

}; /* end of class WebPage */

} /* end of namespace mayu */

#endif /* _WEBPAGE_HH */
