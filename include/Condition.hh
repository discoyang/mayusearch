/* Condition负责管理线程条件变量
 * 编译时需要加上-lpthread
 */

#ifndef _CONDITION_HH
#define _CONDITION_HH

#include <pthread.h>

#include "Noncopyable.hh"

namespace mayu
{
// 前向声明
class MutexLock;

class Condition
: public Noncopyable
{
public:
    Condition(MutexLock &__mutex);

    ~Condition();

    // 阻塞等待条件变量
    void wait();

    // 唤醒单个
    void notify();

    // 唤醒所有
    void notify_all();

private:
    pthread_cond_t _cond;
    
    MutexLock &_mutex;

}; /* end of class Condition */

} /* end of namespace mayu */

#endif /* _CONDITION_HH */
