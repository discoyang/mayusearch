/* TcpConnection负责存储每一个新创建的客户端连接
 * 负责对客户端进行读写
 * 通过回调函数执行业务逻辑
 * 具有对象语义
 */

#ifndef _TCPCONNECTION_HH
#define _TCPCONNECTION_HH

#include <functional>
#include <memory>

#include "InetAddress.hh"
#include "Socket.hh"
#include "SocketIO.hh"

namespace mayu
{
// 前向声明
class EventLoop;
// 前向声明
class TcpConnection;
// 保存Tcp连接的智能指针
using TcpConnectionShptr = std::shared_ptr<TcpConnection>;
// Tcp连接中三个半事件执行的回调函数
using TcpConnectionCallback = std::function<void(const TcpConnectionShptr &)>;

class TcpConnection
: public std::enable_shared_from_this<TcpConnection>
{
public:
    TcpConnection(int __peerfd, EventLoop *__loop);

    ~TcpConnection();

    // 发送信息
    void send(const std::string &__msg) const;

    // 在事件循环（IO线程）中执行发送信息的任务
    void send_in_loop(const std::string &__msg);

    // 接收信息
    std::string recv() const;

    std::string to_string() const;

    bool is_closed() const;

    // 注册三个回调函数
    void set_connection_callback(const TcpConnectionCallback &__cb);

    void set_message_callback(const TcpConnectionCallback &__cb);

    void set_close_callback(const TcpConnectionCallback &__cb);
    
    // 执行三个回调函数
    void handle_connection_callback();

    void handle_message_callback();

    void handle_close_callback();

private:
    // 获取本端网络地址信息
    InetAddress _get_local_addr() const;

    // 获取对端网络地址信息
    InetAddress _get_peer_addr() const;


    Socket _sock;

    SocketIO _sock_io;

    InetAddress _local_addr;

    InetAddress _peer_addr;

    EventLoop *_loop;

    TcpConnectionCallback _on_connection;

    TcpConnectionCallback _on_message;

    TcpConnectionCallback _on_close;

}; /* end of class TcpConnection */

} /* end of namespace mayu */

#endif /* _TCPCONNECTION_HH */
