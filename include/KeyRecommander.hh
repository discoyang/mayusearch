/* KeyRecommander负责整合关键词推荐功能
 */

#ifndef _KEYRECOMMANDER_HH
#define _KEYRECOMMANDER_HH

#include <queue>
#include <set>
#include <string>

#include "TcpConnection.hh"

namespace mayu
{
// 候选结果集类型
using ResultQueType = std::priority_queue<MyResult,
      std::vector<MyResult>, MyCompare>;

class KeyRecommander
{
public:
    KeyRecommander(const std::string &__query, const TcpConnectionShptr &__con);

    ~KeyRecommander();

    // 执行查询
    void execute();

private:
    // 查询索引
    void _query_index_table();

    // 进行计算
    void _statistic(std::set<int> &__iset);

    // 计算最小编辑距离
    int _distance(const std::string &__rhs);

    // 响应客户端的请求
    void _response();

    // 等待查询的单词
    std::string _query_word;

    // 与客户端的Tcp连接
    TcpConnectionShptr _con;

    // 保存候选结果集的优先级队列
    ResultQueType _result_que;
    
}; /* end of class KeyRecommander */

} /* end of namespace mayu */

#endif /* _KEYRECOMMANDER_HH */
