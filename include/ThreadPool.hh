/* ThreadPool管理线程池
 * 全局唯一可以使用单例模式
 */

#ifndef _THREADPOOL_HH
#define _THREADPOOL_HH

/* #include <vector> */
#include <memory>

#include "TaskQueue.hh"
#include "Thread.hh"

namespace mayu
{
// 定义在TaskQueue.hh中的任务类型
using Task = std::function<void()>;

class ThreadPool
{
public:
    ThreadPool(size_t __thread_num, size_t __taskque_capacity);

    ~ThreadPool();

    // 启动线程池
    void start();

    // 退出线程池
    void stop();

    // 添加任务
    void add_task(Task &&__task);

    // 取出任务
    Task get_task();
    
private:
    // 交给工作线程做的任务
    void _thread_func();

    // 子线程数目
    size_t _thread_num;

    // 任务队列大小
    size_t _taskque_capacity;

    // 存储子线程的容器
    std::vector<std::unique_ptr<Thread>> _threads;

    // 任务队列
    TaskQueue _task_que;

    // 线程池标志位 1表示运行 0表示退出
    bool _is_running;

}; /* end of class ThreadPool */

} /* end of namespace mayu */

#endif /* _THREADPOOL_HH */
