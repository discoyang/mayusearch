/* Dictionary管理全局词典
 * 已经实现单例模式
 * 不保证线程安全
 */

#ifndef _DICTIONARY_HH
#define _DICTIONARY_HH

#include <map>
#include <set>
#include <vector>

namespace mayu
{
// 词典类型
using DictType = std::vector<std::pair<std::string, int>>;
// 索引表类型
using IndexTableType = std::map<std::string, std::set<int>>;

class Dictionary
{
public:
    static Dictionary *get_instance();

    static void destroy();

    // 通过词典文件路径初始化词典
    void init(const std::string &__en_dict_path, const std::string &__cn_dict_path,
              const std::string &__en_idx_path, const std::string &__cn_idx_path);

    // 获取词典
    DictType &get_dict();

    // 获取索引表
    IndexTableType &get_index_table();

    // 执行查询
    std::string do_query(std::string __word);

    Dictionary(const Dictionary &__rhs) = delete;
    Dictionary(Dictionary &&__rhs) = delete;

    Dictionary &operator=(const Dictionary &__rhs) = delete;
    Dictionary &operator=(Dictionary &&__rhs) = delete;

private:
    Dictionary();

    ~Dictionary();


    static Dictionary *_instance_ptr;

    // 词典
    DictType _dict;

    // 索引表
    IndexTableType _index_table;

}; /* end of class Dictionary */

} /* end of namespace mayu */

#endif /* _DICTIONARY_HH */
