/* 继承Noncopyable可以实现对象语义
 * Noncopyable是一个抽象类
 */

#ifndef _NONCOPYABLE_HH
#define _NONCOPYABLE_HH

namespace mayu
{
class Noncopyable
{
protected:
    Noncopyable() {}

    ~Noncopyable() {}

    Noncopyable(const Noncopyable &rhs) = delete;

    Noncopyable &operator=(const Noncopyable &rhs) = delete;

}; /* end of class Noncopyable */

} /* end of namespace mayu */

#endif /* _NONCOPYABLE_HH */
