/* MutexLock负责管理线程互斥锁
 * MutexLockGuard利用对象的生命周期进行自动上锁和释放锁的操作
 * 编译时需要加上-lpthread
 */

#ifndef _MUTEXLOCK_HH
#define _MUTEXLOCK_HH

#include <pthread.h>

#include "Noncopyable.hh"

namespace mayu
{
class MutexLock
: public Noncopyable
{
public:
    MutexLock();

    ~MutexLock();

    void lock();

    void unlock();

    pthread_mutex_t *get_ptr();

private:
    pthread_mutex_t _mutex;

}; /* end of class MutexLock */

class MutexLockGuard
{
public:
    MutexLockGuard(MutexLock &__mutex);

    ~MutexLockGuard();

private:
    MutexLock &_mutex;

}; /* end of class MutexLockGuard */

} /* end of namespace mayu */

#endif /* _MUTEXLOCK_HH */
