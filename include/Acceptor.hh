/* Acceptor负责处理等待连接队列
 * 在三次握手成功后创建新连接并返回对应文件描述符
 */

#ifndef _ACCEPTOR_HH
#define _ACCEPTOR_HH

#include "InetAddress.hh"
#include "Socket.hh"

namespace mayu
{
class Acceptor
{
public:
    Acceptor(const std::string &__ip, unsigned short __port);

    ~Acceptor();

    // 使监听套接字处于就绪状态
    void ready() const;

    // 创建新连接并返回文件描述符
    int accept() const;

    // 返回监听套接字的文件描述符
    int get_fd() const;

private:
    void _set_reuse_addr() const;

    void _set_reuse_port() const;

    void _bind() const;

    void _listen() const;
    
    Socket _listensock;

    InetAddress _addr;

}; /* end of class Acceptor */

} /* end of namespace mayu */

#endif /* _ACCEPTOR_HH */
