#ifndef _ECHOSERVER_HH
#define _ECHOSERVER_HH

#include "TcpServer.hh"
#include "ThreadPool.hh"

namespace mayu
{
class EchoServer
{
public:
    EchoServer(size_t __thread_num, size_t __taskque_capacity,
               const std::string &__ip, unsigned short __port);

    ~EchoServer();

    // 开启Echo服务器
    void start();

    // 关闭Echo服务器
    void stop();

private:
    void _on_connection(const TcpConnectionShptr &__con);
    
    void _on_message(const TcpConnectionShptr &__con);
    
    void _on_close(const TcpConnectionShptr &__con);


    ThreadPool _pool;

    TcpServer _server;

}; /* end of class EchoServer */

} /* end of namespace mayu */

#endif /* _ECHOSERVER_HH */
