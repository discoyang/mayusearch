#ifndef _SINGLETON_HH
#define _SINGLETON_HH

#include <pthread.h>
#include <stdlib.h>

namespace mayu
{
template <typename T>
class Singleton
{
public:
    template <typename... Args>
    static T *get_instance(Args... args)
    {
        // 这样写可以保证线程安全吗？
        /* static T __instance; */
        /* return &__instance; */

        if (nullptr == _instance_ptr) {
            _instance_ptr = new T(args...);
            atexit(destroy);   
        }

        return _instance_ptr;
    }

    static void destroy()
    {
        if (_instance_ptr) {
            delete _instance_ptr;
            _instance_ptr = nullptr;
        }
    }

private:
    Singleton();

    ~Singleton();


    static T *_instance_ptr;

    // 这样就能保证线程安全吗？
    static pthread_once_t _once;

}; /* end of class Singleton */

template <typename T>
T *Singleton<T>::_instance_ptr = nullptr;

template <typename T>
pthread_once_t Singleton<T>::_once = PTHREAD_ONCE_INIT;

} /* end of namespace mayu */

#endif /* _SINGLETON_HH */
