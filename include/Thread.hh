/* Thread管理线程
 * Thread具有对象语义
 */

#ifndef _THREAD_HH
#define _THREAD_HH

#include <functional>

#include "Noncopyable.hh"

namespace mayu
{
// 线程回调函数类型
using ThreadCallback = std::function<void()>;

class Thread
: public Noncopyable
{
public:
    Thread(ThreadCallback &&__cb);

    ~Thread();

    void start();

    void stop();
    
private:
    // 线程入口函数
    static void *thread_func(void * __arg);

    pthread_t _thid;

    bool _is_running;

    ThreadCallback _cb;

}; /* end of class Thread */

} /* end of namespace mayu */

#endif /* _THREAD_HH */
