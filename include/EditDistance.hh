#ifndef _EDITDISTANCE_HH
#define _EDITDISTANCE_HH

#include <string>

namespace mayu
{
// 求取一个字符占据的字节数
size_t nBytesCode(const char ch);

// 求取一个字符串的字符长度
size_t length(const std::string &str);

// 中英文通用的最小编辑距离算法
int editDistance(const std::string & lhs, const std::string &rhs);

} /* end of namespace mayu */

#endif /* _EDITDISTANCE_HH */
