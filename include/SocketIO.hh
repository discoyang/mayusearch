/* SocketIO负责对套接字进行读写
 */

#ifndef _SOCKETIO_HH
#define _SOCKETIO_HH

namespace mayu
{
class SocketIO
{
public:
    SocketIO(int __fd);

    ~SocketIO();

    // 接收__len个字符到__buff指向的缓冲区中
    int readn(char *__buff, int __len) const;

    // 接收一行字符到__buff指向的缓冲区中，缓冲区大小为__len
    // 返回接收到的不包括换行符的字符数
    int readline(char *__buff, int __len) const;

    // 从__buff指向的缓冲区中发送__len个字符
    int writen(const char *__buff, int __len) const;

    // 从内核接收区拷贝一份__len个字符的副本到__buff指向的缓冲区
    int readpeek(char *__buff, int __len) const;

private:
    int _fd;

}; /* end of class SocketIO */

} /* end of namespace mayu */

#endif /* _SOCKETIO_HH */
