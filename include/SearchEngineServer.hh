/* SearchEngineServer是搜索引擎整体架构
 * 整合了线程池、Tcp服务器以及所有业务逻辑
 */

#ifndef _SEARCHENGINESERVER_HH
#define _SEARCHENGINESERVER_HH

#include "TcpServer.hh"
#include "ThreadPool.hh"
#include "KeyRecommander.hh"
#include "WebPageSearcher.hh"

namespace mayu
{
class SearchEngineServer
{
public:
    SearchEngineServer(const std::string &__ip = "127.0.0.1",
                       unsigned short __port = 8888,
                       size_t __thread_num = 4,
                       size_t __taskque_capacity = 10);

    ~SearchEngineServer();

    // 开启搜索引擎服务器
    void start();

    // 关闭搜索引擎服务器
    void stop();

private:
    // 分别对应三个半事件的回调函数
    void _on_connection(const TcpConnectionShptr &__con);
    
    void _on_message(const TcpConnectionShptr &__con);
    
    void _on_close(const TcpConnectionShptr &__con);

    // 线程要执行的具体任务
    void _thread_task(const TcpConnectionShptr &__con,
                      const std::string &__msg);



    // 线程池模块
    ThreadPool _pool;

    // Tcp通信模块
    TcpServer _server;

    /* 以下模块放在这里无法初始化 */
    /* // 关键字推荐模块 */
    /* KeyRecommander _key_recommander; */

    /* // 网页查询模块 */
    /* WebPageSearcher _web_searcher; */

}; /* end of class SearchEngineServer */

} /* end of namespace mayu */

#endif /* _SEARCHENGINESERVER_HH */
