/* WebPageQuery负责网页查询功能的内部逻辑
 * 存储了网页库、偏移库和倒排索引库
 * 
 * 为了防止重复加载三个库，我觉得应该设成单例类，但开发文档里没写
 */

#ifndef _WEBPAGEQUERY_HH
#define _WEBPAGEQUERY_HH

#include <unordered_map>

#include "WebPage.hh"

namespace mayu
{
// 网页库类型
using PageLibType = std::unordered_map<int, WebPage>;
// 偏移库类型
using OffsetLibType = std::unordered_map<int, std::pair<int, int>>;
// 倒排索引表类型
using InvertIndexTableType = std::unordered_map<std::string,
      std::set<std::pair<int, double>>>;
// 查询结果类型
using QueryResultVecType = std::vector<std::pair<int, std::vector<double>>>;

class WebPageQuery
{
public:
    WebPageQuery();

    ~WebPageQuery();
    
    // 外部接口，执行查询，返回结果
    std::string do_query(const std::string &__keyword);

    // 加载库文件
    void _load_library();

    // 计算查询词的权重值
    std::vector<double> _get_query_words_weight_vector
        (std::vector<std::string> &__query_words);

    // 执行查询
    bool _execute_query(const std::vector<std::string> &__query_words,
                       QueryResultVecType &__result_vec);

    // 生成Json格式结果
    std::string _create_json(std::vector<int> &__doc_id_vec,
                            const std::vector<std::string> &__query_words);

    std::string _return_no_answer();

private:
    // jieba分词库对象
    WordSegmentation _jieba;

    // 网页库
    PageLibType _page_lib;

    // 偏移库
    OffsetLibType _offset_lib;

    // 倒排索引库
    InvertIndexTableType _invert_index_table;

}; /* end of class WebPageQuery */

} /* end of namespace mayu */

#endif /* _WEBPAGEQUERY_HH */
