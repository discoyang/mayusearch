/* EventLoop负责管理整个事件循环
 */

#ifndef _EVENTLOOP_HH
#define _EVENTLOOP_HH

#include <sys/epoll.h>
#include <sys/eventfd.h>

#include <map>
#include <vector>

#include "Acceptor.hh"
#include "MutexLock.hh"
#include "TcpConnection.hh"

namespace mayu
{
// 在事件循环中负责消息发送的类型
using EventLoopCallback = std::function<void()>;

class EventLoop
{
public:
    EventLoop(Acceptor &__acceptor);

    ~EventLoop();

    // 开启事件循环
    void loop();

    // 结束事件循环
    void unloop();

    // 设置三个回调函数
    void set_connection_callback(TcpConnectionCallback &&__cb);

    void set_message_callback(TcpConnectionCallback &&__cb);

    void set_close_callback(TcpConnectionCallback &&__cb);
    
    // 接收要在事件循环中发送的信息
    void run_in_loop(EventLoopCallback &&__cb);

private:
    // 调用epoll_wait函数进行阻塞监听
    void _epoll_wait();

    // 处理新连接
    void _handle_new_connection();

    // 处理消息发送
    void _handle_message(int __fd);

    // 创建epoll实例
    int _create_epollfd();

    // 监听读事件
    void _add_epollin_fd(int __fd);

    // 取消监听读事件
    void _del_epollin_fd(int __fd);
    
    
    int _create_eventfd();

    // 读eventfd，处理回调函数
    void _read_evfd();

    // 写eventfd，通知事件循环处理回调函数
    void _write_evfd();

    // 处理信息发送任务（回调函数）
    void _handle_event_loop_callback();
    

    const int _epfd;

    const int _evfd;

    // 存储epoll_wait返回的事件
    std::vector<struct epoll_event> _epoll_events;

    // 循环标志位 1表示循环正在进行 0表示循环停止
    bool _is_running;

    // 负责建立连接的对象
    Acceptor &_acceptor;

    // 存储已经建立的Tcp连接
    std::map<int, TcpConnectionShptr> _connections;

    TcpConnectionCallback _on_connection;

    TcpConnectionCallback _on_message;

    TcpConnectionCallback _on_close;

    std::vector<EventLoopCallback> _callback_vec;

    MutexLock _mutex;

}; /* end of class EventLoop */

} /* end of namespace mayu */

#endif /* _EVENTLOOP_HH */
