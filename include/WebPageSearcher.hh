/* WebPageSearcher负责整合网页搜索功能
 */

#ifndef _WEBPAGESEARCHER_HH
#define _WEBPAGESEARCHER_HH

#include <string>

#include "TcpConnection.hh"

namespace mayu
{
class WebPageSearcher
{
public:
    WebPageSearcher(std::string __keys, const TcpConnectionShptr &__con);

    ~WebPageSearcher();

    // 执行查询
    void do_query();

private:
    // 查询关键词
    std::string _keyword;

    // 已经建立的Tcp连接
    TcpConnectionShptr _con;

}; /* end of class WebPageSearcher */

} /* end of namespace mayu */

#endif /* _WEBPAGESEARCHER_HH */
