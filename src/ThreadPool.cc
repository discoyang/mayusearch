#include "../include/ThreadPool.hh"

#include <unistd.h>

namespace mayu
{
ThreadPool::ThreadPool(size_t __thread_num, size_t __taskque_capacity)
: _thread_num(__thread_num)
, _taskque_capacity(__taskque_capacity)
, _task_que(_taskque_capacity)
, _is_running(true)
{
    _threads.reserve(_thread_num);
}

ThreadPool::~ThreadPool()
{
    if (_is_running) {
        _is_running = false;
        stop();
    }
}

// 启动线程池
void
ThreadPool::start()
{
    for (size_t __idx = 0; __idx != _thread_num; ++__idx) {
        // 绑定回调函数的参数
        ThreadCallback __cb = std::bind(&ThreadPool::_thread_func, this);
        
        // 申请内存存放子线程数据
        std::unique_ptr<Thread> __up(new Thread(std::move(__cb)));

        // 启动子线程
        __up->start();

        // 把子线程智能指针放入数组
        _threads.push_back(std::move(__up));
    }
}

// 退出线程池
void ThreadPool::stop()
{
    // 先判断任务是否执行完毕
    // 执行完毕后退出子线程
    while (!_task_que.is_empty()) {
        sleep(1);
    }

    _is_running = false;
    _task_que.stop();

    for (auto &__up : _threads) {
        // 退出子线程
        __up->stop();
    }
}

// 添加任务
void
ThreadPool::add_task(Task &&__task)
{
    if (__task) {
        _task_que.push(std::move(__task));
    }
}

// 取出任务
Task
ThreadPool::get_task()
{
    return _task_que.pop();
}

// 交给工作线程做的任务
void
ThreadPool::_thread_func()
{
    while (_is_running) {
        Task __task = get_task();
        if (__task) {
            __task();
        }
    }
}

} /* end of namespace mayu */
