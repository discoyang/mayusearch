#include "../include/EventLoop.hh"

#include <unistd.h>

#include <iostream>

namespace mayu
{
EventLoop::EventLoop(Acceptor &__acceptor)
: _epfd(_create_epollfd())
, _evfd(_create_eventfd())
, _epoll_events(1024)
, _is_running(false)
, _acceptor(__acceptor)
{
    // 监听listenfd
    _add_epollin_fd(_acceptor.get_fd());
    
    // 监听eventfd
    _add_epollin_fd(_evfd);
}

EventLoop::~EventLoop()
{
    ::close(_epfd);
    ::close(_evfd);
}

// 开启事件循环
void
EventLoop::loop()
{
    _is_running = true;
    
    while (_is_running) {
        _epoll_wait();
    }
}

// 结束事件循环
void
EventLoop::unloop()
{
    _is_running = false;
}

// 调用epoll_wait函数进行阻塞监听
void
EventLoop::_epoll_wait()
{
    int __nready = 0;
    do {
        __nready = ::epoll_wait(_epfd, &*_epoll_events.begin(),
                              _epoll_events.size(), 5000);
    } while (-1 == __nready && EINTR == errno);

    if (-1 == __nready) {
        // epoll_wait出错
        ::perror("epoll_wait");
    } else if (0 == __nready) {
        // 超时，没有新可读事件产生

        std::cout << ">> epoll_wait timeout" << std::endl;
    } else {
        if (static_cast<int>(_epoll_events.size()) == __nready) {
            // 对保存struct epoll_event的容器进行扩容
            _epoll_events.resize(2 * __nready);
        }

        for (int __idx = 0; __idx < __nready; ++__idx) {
            // 保存当前可以进行读操作的文件描述符
            int __fd = _epoll_events[__idx].data.fd;
            
            if (_acceptor.get_fd() == __fd) {
                // 有新连接可以建立
                
                // 由于当前只监听了读事件，这里可以不进行if判断
                if (_epoll_events[__idx].events & EPOLLIN) {
                    _handle_new_connection();
                }
            } else if (_evfd == __fd) {
                // 监听的_evfd有事件发生，即有信息要发回客户端

                // 由于当前只监听了读事件，这里可以不进行if判断
                if (_epoll_events[__idx].events & EPOLLIN) {
                    _read_evfd();
                    // 遍历vector，执行回调函数，完成把信息发回客户端的任务
                    _handle_event_loop_callback();
                }
            } else {
                // 客户端发来信息
                
                // 由于当前只监听了读事件，这里可以不进行if判断 
                if (_epoll_events[__idx].events & EPOLLIN) {
                    _handle_message(__fd);
                }                  
            }
        } /* end of for */
    } /* end of if */
}

// 处理新连接
void
EventLoop::_handle_new_connection()
{
    int __peerfd = _acceptor.accept();

    if (-1 == __peerfd) {
        ::perror("peerfd");
        return;
    }

    // 将该文件描述符放在红黑树上监听
    _add_epollin_fd(__peerfd);

    TcpConnectionShptr __con(new TcpConnection(__peerfd, this));
    // 注册三个事件
    __con->set_connection_callback(_on_connection);
    __con->set_message_callback(_on_message);
    __con->set_close_callback(_on_close);

    _connections.insert(std::make_pair(__peerfd, __con));

    __con->handle_connection_callback();
}

// 处理消息发送
void
EventLoop::_handle_message(int __fd)
{
    auto __it = _connections.find(__fd);

    if (__it != _connections.end()) {
        // 该连接存在

        // 判断是否有内容可读
        bool __flag = __it->second->is_closed();
        if (__flag) {
            // 处理连接的断开
            __it->second->handle_close_callback();
            // 将文件描述符从红黑树上删除
            _del_epollin_fd(__fd);
            // 将Tcp连接从map中删除
            _connections.erase(__it);
        } else {    
            // 进行正常的读写操作
            __it->second->handle_message_callback();
        }
    } else {
        // 该连接不存在
        std::cout << "该连接不存在" << std::endl;
    }
}

// 创建epoll实例
int
EventLoop::_create_epollfd()
{
    int __epfd = ::epoll_create1(0);
    if (-1 == _epfd) {
        ::perror("epoll_create1");
    }

    return __epfd;
}

// 监听读事件
void
EventLoop::_add_epollin_fd(int __fd)
{
    struct epoll_event __event;
    __event.events = EPOLLIN;
    __event.data.fd = __fd;

    int __ret = ::epoll_ctl(_epfd, EPOLL_CTL_ADD, __fd, &__event);
    if (-1 == __ret) {
        ::perror("epoll_ctl add");
    }
}

// 取消监听读事件
void
EventLoop::_del_epollin_fd(int __fd)
{
    struct epoll_event __event;
    __event.events = EPOLLIN;
    __event.data.fd = __fd;

    int __ret = ::epoll_ctl(_epfd, EPOLL_CTL_DEL, __fd, &__event);
    if (-1 == __ret) {
        ::perror("epoll_ctl del");
    }
}

// 设置三个回调函数
void
EventLoop::set_connection_callback(TcpConnectionCallback &&__cb)
{
    _on_connection = std::move(__cb);
}

void
EventLoop::set_message_callback(TcpConnectionCallback &&__cb)
{
    _on_message = std::move(__cb);
}

void
EventLoop::set_close_callback(TcpConnectionCallback &&__cb)
{
    _on_close = std::move(__cb);
}

int
EventLoop::_create_eventfd()
{
    int __evfd = ::eventfd(10, 0);
    if (-1 == __evfd) {
        ::perror("eventfd");
    }

    return __evfd;
}

// 读event，处理回调函数
void
EventLoop::_read_evfd()
{
    uint64_t __one = 1;
    int __ret = ::read(_evfd, &__one, sizeof(__one));
    if (sizeof(__one) != __ret) {
        ::perror("read");
    }
}

// 写eventfd，通知事件循环处理回调函数
void
EventLoop::_write_evfd()
{
    uint64_t __one = 1;
    int __ret = ::write(_evfd, &__one, sizeof(__one));
    if (sizeof(__one) != __ret) {
        ::perror("write");
    }
}

// 处理信息发送任务（回调函数）
void
EventLoop::_handle_event_loop_callback()
{
    std::vector<EventLoopCallback> __tmp;
   
    {
        MutexLockGuard __autolock(_mutex);
        __tmp.swap(_callback_vec);
    }

    for (auto &__cb : __tmp) {
        __cb();
    }
}

// 接收要在事件循环中发送的信息
void EventLoop::run_in_loop(EventLoopCallback &&__cb)
{
    {
        MutexLockGuard __autolock(_mutex);
        _callback_vec.push_back(std::move(__cb));
    }

    _write_evfd();
}

} /* end of namespace mayu */
