#include "../include/Dictionary.hh"

#include <fstream>
#include <iostream>
#include <sstream>

namespace mayu
{
Dictionary *
Dictionary::get_instance()
{
    if (nullptr == _instance_ptr) {
        _instance_ptr = new Dictionary();
        atexit(destroy);
    } 

    return _instance_ptr;
}

void
Dictionary::destroy()
{
    if (_instance_ptr) {
        delete _instance_ptr;
        _instance_ptr = nullptr;
    }
}

void
Dictionary::init(const std::string &__en_dict_path, const std::string &__cn_dict_path,
                 const std::string &__en_idx_path, const std::string &__cn_idx_path)
{
    std::ifstream __en_dict_ifs(__en_dict_path);
    if (!__en_dict_ifs.good()) {
        std::cerr << "open" << __en_dict_path << " fail!" << std::endl;
        return;
    }

    std::ifstream __cn_dict_ifs(__cn_dict_path);
    if (!__cn_dict_ifs.good()) {
        std::cerr << "open" << __cn_dict_path << " fail!" << std::endl;
        return;
    }

    std::ifstream __en_idx_ifs(__en_idx_path);
    if (!__en_idx_ifs.good()) {
        std::cerr << "open" << __en_idx_path << " fail!" << std::endl;
        return;
    }

    std::ifstream __cn_idx_ifs(__cn_idx_path);
    if (!__cn_idx_ifs.good()) {
        std::cerr << "open" << __cn_idx_path << " fail!" << std::endl;
        return;
    }
    
    std::string __line;
    std::string __word;
    int __frequency;
    size_t __idx;

    // 插入英文词典
    while (getline(__en_dict_ifs, __line)) {
        std::istringstream __iss(__line);
        __iss >> __word >> __frequency;
        _dict.push_back(make_pair(__word, __frequency));
    }
    size_t __cn_dict_offset = _dict.size();

    // 插入中文词典
    while (getline(__cn_dict_ifs, __line)) {
        std::istringstream __iss(__line);
        __iss >> __word >> __frequency;
        _dict.push_back(make_pair(__word, __frequency));
    }
    
    // 插入英文索引
    while (getline(__en_idx_ifs, __line)) {
        std::istringstream __iss(__line);
        __iss >> __word;
        while (__iss >> __idx) {
            _index_table[__word].insert(__idx);
        }
    }

    // 插入中文索引
    while (getline(__cn_idx_ifs, __line)) {
        std::istringstream __iss(__line);
        __iss >> __word;
        while (__iss >> __idx) {
            _index_table[__word].insert(__idx + __cn_dict_offset);
        }
    }
    
    __en_dict_ifs.close();
    __cn_dict_ifs.close();
    __en_idx_ifs.close();
    __cn_idx_ifs.close();
    
}

DictType &
Dictionary::get_dict()
{
    return get_instance()->_dict;
}

IndexTableType &
Dictionary::get_index_table()
{
    return get_instance()->_index_table;
}

std::string
Dictionary::do_query(std::string __word)
{

}

Dictionary::Dictionary()
{

}

Dictionary::~Dictionary()
{

}

Dictionary *Dictionary::_instance_ptr = nullptr;

} /* end of namespace mayu */
