#include "../include/Acceptor.hh"

namespace mayu
{
Acceptor::Acceptor(const std::string &__ip, unsigned short __port)
: _listensock()
, _addr(__ip, __port)
{}

Acceptor::~Acceptor()
{}

// 使监听套接字处于就绪状态
void
Acceptor::ready() const 
{
    _set_reuse_addr();
    _set_reuse_port();

    _bind();
    _listen();
}

// 创建新连接并返回文件描述符
int
Acceptor::accept() const
{
    int __peerfd = ::accept(get_fd(), nullptr, nullptr);

    if (__peerfd < 0) {
        ::perror("accept");
    }

    return __peerfd;
}

// 返回监听套接字的文件描述符
int
Acceptor::get_fd() const
{
    return _listensock.get_fd();
}

void
Acceptor::_set_reuse_addr() const
{
    const int __optval = 1;
    int __ret = ::setsockopt(get_fd(), SOL_SOCKET, SO_REUSEADDR,
                             &__optval, sizeof(__optval));

    if (-1 == __ret) {
        ::perror("setsockopt");
    }
}

void
Acceptor::_set_reuse_port() const
{
    const int __optval = 1;
    int __ret = ::setsockopt(get_fd(), SOL_SOCKET, SO_REUSEPORT,
                             &__optval, sizeof(__optval));

    if (-1 == __ret) {
        ::perror("setsockopt");
    }
}

void
Acceptor::_bind() const
{
    int __ret = ::bind(get_fd(),
                       reinterpret_cast<const struct sockaddr *>(_addr.get_sockaddr_ptr()),
                       sizeof(struct sockaddr));
    
    if (-1 == __ret) {
        ::perror("bind");
    }
}

void
Acceptor::_listen() const
{
    int __ret = ::listen(get_fd(), 128);

    if (-1 == __ret) {
        ::perror("listen");
    }
}

} /* end of namespace mayu */
