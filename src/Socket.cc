#include "../include/Socket.hh"

#include <fcntl.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

namespace mayu
{
Socket::Socket()
: _fd(::socket(AF_INET, SOCK_STREAM, 0))
{
    if (_fd < 0) {
        ::perror("socket");
    }
}

Socket::Socket(int __fd)
: _fd(__fd)
{}

Socket::~Socket()
{
    ::close(_fd);
}

int
Socket::get_fd() const
{
    return _fd;
}

// 关闭写端
void
Socket::shutdown_write() const
{
    ::shutdown(_fd, SHUT_WR);
}

// 把套接字属性设置为非阻塞
void
Socket::set_nonblock() const
{
    int flags = ::fcntl(_fd, F_GETFL, 0);
    flags |= O_NONBLOCK;
    ::fcntl(_fd, F_SETFL, flags);
}

} /* end of namespace mayu */
