#include "../include/SocketIO.hh"

#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>


namespace mayu
{
SocketIO::SocketIO(int __fd)
: _fd(__fd)
{}

SocketIO::~SocketIO()
{}

// 接收__len个字符到__buff指向的缓冲区中
int
SocketIO::readn(char *__buff, int __len) const
{
    char *__buffptr = __buff;
    int __left = __len;
    int __ret = 0;

    while (__left > 0) {
        __ret = ::read(_fd, __buffptr, __len);

        if (-1 == __ret && EINTR == errno) {
            continue;
        } else if (-1 == __ret) {
            ::perror("read");
            break;
        } else if (0 == __ret) {
            break;
        } else {
            __left -= __ret;
            __buffptr += __ret;
        }
    } /* end of while */

    return __len - __left;
}

// 接收一行字符到__buff指向的缓冲区中，缓冲区大小为__len，
// 返回接收到的不包括换行符的字符数
int
SocketIO::readline(char *__buff, int __len) const
{
    char *__buffptr = __buff;
    int __left = __len - 1;
    int __ret = 0;

    while (__left > 0) {
        __ret = readpeek(__buffptr, __left);

        if (-1 == __ret) {
            break;
        } else if (0 == __ret) {
            break;
        } else {
            for (int __idx = 0; __idx < __ret; ++__idx) {
                if ('\n' == __buffptr[__idx]) {
                    readn(__buffptr, __idx + 1);
                    __buffptr += __idx;
                    // 将换行符替换为字符串结束符
                    *__buffptr = '\0';

                    return __len - 1 - __left + __idx;
                }
            } /* end of for */

            readn(__buffptr, __ret);
            __buffptr += __ret;
            __left -= __ret;
        }
    } /* end of while */

    // 在字符串末尾添加字符串结束符
    *__buffptr = '\0';

    return __len - 1 - __left;
}

// 从__buff指向的缓冲区中发送__len个字符
int
SocketIO::writen(const char *__buff, int __len) const
{
    const char *__buffptr = __buff;
    int __left = __len;
    int __ret = 0;

    while (__left > 0) {
        __ret = ::write(_fd, __buffptr, __left);
        if (-1 == __ret && EINTR == errno) {
            continue;
        } else if (-1 == __ret) {
            ::perror("write");
            break;
        } else if (0 == __ret) {
            break;
        } else {
            __buffptr += __ret;
            __left -= __ret;
        }
    } /* end of while */

    return __len - __left;
}

// 从内核接收区拷贝一份__len个字符的副本到__buff指向的缓冲区
int
SocketIO::readpeek(char *__buff, int __len) const
{
    int __ret = 0;
    while (__len > 0) {
        __ret = ::recv(_fd, __buff, __len, MSG_PEEK);
    
        if (-1 == __ret && EINTR == errno) {
            continue;
        } else if (-1 == __ret) {
            ::perror("recv");
            break;
        } else {
            break;
        }
    }

    return __ret;
} 

} /* end of namespace mayu */
