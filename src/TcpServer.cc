#include "../include/TcpServer.hh"

namespace mayu
{
TcpServer::TcpServer(const std::string &__ip, unsigned short __port)
: _acceptor(__ip, __port)
, _loop(_acceptor)
{}

TcpServer::~TcpServer()
{}

void
TcpServer::start()
{
    _acceptor.ready();
    _loop.loop();
}

void
TcpServer::stop()
{
    _loop.unloop();
}

void
TcpServer::set_all_callback(TcpConnectionCallback &&__on_connection,
                            TcpConnectionCallback &&__on_message,
                            TcpConnectionCallback &&__on_close)
{
    _loop.set_connection_callback(std::move(__on_connection));
    _loop.set_message_callback(std::move(__on_message));
    _loop.set_close_callback(std::move(__on_close));
}

} /* end of namespace mayu */
