#include "../include/WebPageQuery.hh"

namespace mayu
{
WebPageQuery::WebPageQuery()
{
    // to do ...



}

WebPageQuery::~WebPageQuery()
{
    // to do ...




}


// 外部接口，执行查询，返回结果
std::string
WebPageQuery::do_query(const std::string &__keyword)
{
    // to do ...



}

// 加载库文件
void
WebPageQuery::_load_library()
{
    // to do ...
    




}

// 计算查询词的权重值
std::vector<double>
WebPageQuery::_get_query_words_weight_vector
    (std::vector<std::string> &__query_words)
{
    //to do ...




}

// 执行查询
bool
WebPageQuery::_execute_query(const std::vector<std::string> &__query_words,
                             QueryResultVecType &__result_vec)
{
    // to do ...





}

// 生成Json格式结果
std::string
WebPageQuery::_create_json(std::vector<int> &__doc_id_vec,
                           const std::vector<std::string> &__query_words)
{
    // to do ...
    




}

// ？？作用不清楚
std::string
WebPageQuery::_return_no_answer()
{
    // to do ...
    



}

} /* end of namespace mayu */
