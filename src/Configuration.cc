#include "../include/Configuration.hh"

#include <fstream>
#include <iostream>

namespace mayu
{
Configuration *
Configuration::get_instance()
{
    if (nullptr == _instance_ptr) {
        _instance_ptr = new Configuration();
        atexit(destroy);
    }

    return _instance_ptr;
}

void
Configuration::destroy()
{
    if (_instance_ptr) {
        delete _instance_ptr;
        _instance_ptr = nullptr;
    }   
}

// 通过配置文件路径初始化配置内容
void
Configuration::init(const std::string &__filepath)
{
    std::ifstream __ifs(__filepath);

    if (!__ifs.good()) {
        std::cerr << "open " << __filepath << " error" << std::endl;
        return;
    }

    std::string __line;

    while (getline(__ifs, __line)) {
        std::string __key, __value;
        if (_config_map.find(__key) == _config_map.end()) {
            _config_map[__key] = __value;
        } else {
            std::cout << __key << " has existed!" << std::endl;
        }

        __line.clear();
    }

    __ifs.close();

    _init_stop_word_set();
}

// 获取存放配置文件内容的map
ConfigMapType &
Configuration::get_config_map()
{
    return get_instance()->_config_map;
}

StopWordSetType &
Configuration::get_stop_word_set()
{
    return get_instance()->_stop_word_set;
}

Configuration::Configuration()
{

}

Configuration::~Configuration()
{

}

void
Configuration::_init_stop_word_set()
{
    std::ifstream __if_stop_words_utf8(_config_map[map_key.STOP_WORD_PATH]);

    if (!__if_stop_words_utf8.good()) {
        std::cerr << "if_stop_words_utf8 is not good";
        return;
    }

    std::string __stop_word_utf8;
    while (__if_stop_words_utf8 >> __stop_word_utf8) {
        _stop_word_set.insert(__stop_word_utf8);
    }

#if 0
    for (auto &__tmp : _stop_word_set) {
        std::cout << __tmp << std::endl;
    }
#endif
}

Configuration *Configuration::_instance_ptr = nullptr;

} /* end of namespace mayu */
