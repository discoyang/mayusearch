#include "../include/Condition.hh"

#include <stdio.h>

#include "../include/MutexLock.hh"

namespace mayu
{
Condition::Condition(MutexLock &__mutex)
: _mutex(__mutex)
{
    int __ret = ::pthread_cond_init(&_cond, nullptr);

    if (__ret) {
        ::perror("pthread_cond_init");
    }
}

Condition::~Condition()
{
    int __ret = ::pthread_cond_destroy(&_cond);

    if (__ret) {
        ::perror("pthread_cond_destroy");
    }
}

// 阻塞等待条件变量
void
Condition::wait()
{
    int __ret = ::pthread_cond_wait(&_cond, _mutex.get_ptr());

    if (__ret) {
        ::perror("pthread_cond_wait");
    }
}

// 唤醒单个
void
Condition::notify()
{
    int __ret = ::pthread_cond_signal(&_cond);

    if (__ret) {
        ::perror("pthread_cond_signal");
    }
}

// 唤醒所有
void
Condition::notify_all()
{
    int __ret = ::pthread_cond_broadcast(&_cond);

    if (__ret) {
        ::perror("pthread_cond_broadcast");
    }
}

} /* end of namespace mayu */
