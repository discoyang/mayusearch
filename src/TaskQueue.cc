#include "../include/TaskQueue.hh"

namespace mayu
{
TaskQueue::TaskQueue(size_t __capacity)
: _capacity(__capacity)
, _que()
, _mutex()
, _not_full(_mutex)
, _not_empty(_mutex)
, _is_running(true)
{}

TaskQueue::~TaskQueue()
{}

bool
TaskQueue::is_empty() const
{
    return 0 == _que.size();
}

bool
TaskQueue::is_full() const
{
    return _capacity == _que.size();
}

void
TaskQueue::push(Task &&__task)
{
    {
        MutexLockGuard __auto_lock(_mutex);

        // while防止虚假唤醒
        while (_is_running && is_full()) {
            _not_full.wait();
        }

        if (!_is_running) {
            // 主线程已退出，无法添加新任务
            return;
        }
         _que.push(std::move(__task));
    }

    _not_empty.notify(); 
}

Task
TaskQueue::pop()
{
    Task __tmp;
    {
        MutexLockGuard __auto_lock(_mutex);

        // while防止虚假唤醒
        while (_is_running && is_empty()) {
            // wait执行分两部分：
            // 上半部：排队、释放锁、睡眠
            // 下半部：被唤醒、上锁、返回
            _not_empty.wait();
        }
    
        if (!_is_running) {
            return nullptr;
        }
            
        __tmp = _que.front();
        _que.pop();
    }
    
    _not_empty.notify();

    return __tmp;
}

// 唤醒所有阻塞线程
void
TaskQueue::stop()
{
    _is_running = false;
    _not_full.notify_all();
    _not_empty.notify_all();
}

} /* end of namespace mayu */
