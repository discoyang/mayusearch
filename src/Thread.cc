#include "../include/Thread.hh"

#include "pthread.h"

namespace mayu
{
Thread::Thread(ThreadCallback &&__cb)
: _thid(0)
, _is_running(false)
, _cb(__cb)
{}

Thread::~Thread()
{
    if (_is_running) {
        _is_running = false;
        int __ret = ::pthread_detach(_thid);

        if (__ret) {
            ::perror("pthread_detach");
        }
    }
}

void
Thread::start()
{
    if (!_is_running) {
        _is_running = true;
        int __ret = ::pthread_create(&_thid, nullptr,
                                     thread_func, this);

        if (__ret) {
            ::perror("pthread_create");
        }
    }
}

void
Thread::stop()
{
    if (_is_running) {
        _is_running = false;
        int __ret = ::pthread_join(_thid, nullptr);

        if (__ret) {
            ::perror("pthread_join");
        }
    }
}

void *
Thread::thread_func(void * __arg)
{
    Thread *__threadptr = static_cast<Thread *>(__arg);

    if (__threadptr) {
        // 执行回调函数
        __threadptr->_cb();
    }

    ::pthread_exit(nullptr);
}

} /* end of namespace mayu */
