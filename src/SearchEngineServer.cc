#include "../include/SearchEngineServer.hh"

#include "../include/KeyRecommander.hh"
#include "../include/WebPageSearcher.hh"

namespace mayu
{
SearchEngineServer::SearchEngineServer(const std::string &__ip,
                                       unsigned short __port,
                                       size_t __thread_num,
                                       size_t __taskque_capacity)
: _pool(__thread_num, __taskque_capacity)
, _server(__ip, __port)
{

}




} /* end of namespace mayu */
