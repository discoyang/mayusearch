#include "../include/TcpConnection.hh"

#include <sstream>

#include "../include/EventLoop.hh"

namespace mayu
{
constexpr static int BUFFMAXSIZE = 65535;

TcpConnection::TcpConnection(int __peerfd, EventLoop *__loop)
: _sock(__peerfd)
, _sock_io(__peerfd)
, _local_addr(_get_local_addr())
, _peer_addr(_get_peer_addr())
, _loop(__loop)
{}

TcpConnection::~TcpConnection()
{}

// 发送信息
void
TcpConnection::send(const std::string &__msg) const
{
    _sock_io.writen(__msg.c_str(), __msg.size());
}

// 在事件循环（IO线程）中执行发送信息的任务
void
TcpConnection::send_in_loop(const std::string &__msg)
{
    _loop->run_in_loop(std::bind(&TcpConnection::send, this, __msg));
}

// 接收信息
std::string
TcpConnection::recv() const
{
    char __buff[BUFFMAXSIZE] = {0};
    _sock_io.readline(__buff, BUFFMAXSIZE);

    return std::string(__buff);
}

std::string
TcpConnection::to_string() const
{
    std::ostringstream __oss;
    __oss << _local_addr.get_ip()   << ":"
          << _local_addr.get_port() << "-->"
          << _peer_addr.get_ip()    << ":"
          << _peer_addr.get_port();

    return __oss.str();
}

bool
TcpConnection::is_closed() const
{
    // 测试缓冲区的大小不重要
    // 从连接中接收字符数为零即说明对端已经关闭
    constexpr static int BUFFTESTCLOSED = 10;
    char __buff[BUFFTESTCLOSED] = {0};

    int __ret = _sock_io.readpeek(__buff, BUFFTESTCLOSED);

    return 0 == __ret;
}

// 注册三个回调函数
void
TcpConnection::set_connection_callback(const TcpConnectionCallback &__cb)
{
    _on_connection = __cb;
}

void
TcpConnection::set_message_callback(const TcpConnectionCallback &__cb)              
{
    _on_message = __cb;
}

void
TcpConnection::set_close_callback(const TcpConnectionCallback &__cb)
{
    _on_close = __cb;
}

// 执行三个回调函数
void
TcpConnection::handle_connection_callback()
{
    if (_on_connection) {
        _on_connection(shared_from_this());
    }
}

void
TcpConnection::handle_message_callback()
{
    if (_on_message) {
        _on_message(shared_from_this());
    }
}

void
TcpConnection::handle_close_callback()
{
    if (_on_close) {
        _on_close(shared_from_this());
    }
}

// 获取本端网络地址信息
InetAddress
TcpConnection::_get_local_addr() const
{
    struct sockaddr_in __addr;
    socklen_t __len = sizeof(struct sockaddr_in);
    int __ret = ::getsockname(_sock.get_fd(),
                              reinterpret_cast<struct sockaddr *>(&__addr),
                              &__len);

    if (-1 == __ret) {
        ::perror("getsockname");
    }
    
    return InetAddress(__addr);
}

// 获取对端网络地址信息
InetAddress
TcpConnection::_get_peer_addr() const
{
    struct sockaddr_in __addr;
    socklen_t __len = sizeof(struct sockaddr_in);
    int __ret = ::getpeername(_sock.get_fd(),
                              reinterpret_cast<struct sockaddr *>(&__addr),
                              &__len);

    if (-1 == __ret) {
        ::perror("getpeername");
    }
    
    return InetAddress(__addr);

}

} /* end of namespace mayu */
