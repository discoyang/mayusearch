#include "../include/EchoServer.hh"
#include "../include/SearchEngineServer.hh"

void echo_server_test()
{
    mayu::EchoServer echo_server(4, 10, "127.0.0.1", 8888);
    echo_server.start();
}

void search_engine_server_test()
{
    mayu::SearchEngineServer search_engine_server("127.0.0.1", 8888);
    search_engine_server.start();
}

int main(int argc, char *argv[])
{
    /* echo_server_test(); */

    search_engine_server_test();

    return 0;
}
