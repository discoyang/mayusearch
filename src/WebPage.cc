#include "../include/WebPage.hh"

namespace mayu
{
WebPage::WebPage(std::string &__doc, Configuration &__config,
                 WordSegmentation &__jieba)
{
    // to do ...



}

WebPage::~WebPage()
{
    // to do ...

}

// 获取文档的docid
int
WebPage::get_doc_id()
{
    // to do ...



}

// 获取文档的标题
std::string
WebPage::get_title()
{
    // to do ...




}

// 获取文档的摘要
std::string
WebPage::summary(const std::vector<std::string> &__query_words)
{
    // to do ...
    





}

// 获取文档的词频统计map
std::map<std::string, int> &
WebPage::get_word_map()
{
    // to do ...
    





}

// 对格式化文档进行处理
void
WebPage::process_doc(const std::string &__doc, Configuration &__config,
                       WordSegmentation &__jieba)
{
    // to do ...
    




}

// 求取文档的topk词集
void
WebPage::calc_topk(std::vector<std::string> &__words_vec, int k,
                   StopWordSetType &__stop_word_set)
{
    // to do ...
    




}

// 判断两篇文档是否相等
bool operator==(const WebPage &__lhs, const WebPage &__rhs)
{
    // to do ...
    


}

// 对文档按_doc_id进行排序
bool operator<(const WebPage &__lhs, const WebPage &__rhs)
{
    // to do ...
    


}

} /* end of namespace mayu */
