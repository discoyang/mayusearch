#include "../include/InetAddress.hh"

#include <string.h>

namespace mayu
{
InetAddress::InetAddress(const std::string &__ip, const unsigned short __port)
{
    ::memset(&_addr, 0, sizeof(_addr));
    _addr.sin_family = AF_INET;
    _addr.sin_port = ::htons(__port);
    _addr.sin_addr.s_addr = ::inet_addr(__ip.c_str());
}

InetAddress::InetAddress(const struct sockaddr_in &__addr)
: _addr(__addr)
{}

InetAddress::~InetAddress()
{}

std::string
InetAddress::get_ip() const
{
    return std::string(::inet_ntoa(_addr.sin_addr));
}

unsigned short
InetAddress::get_port() const
{
    return ::ntohs(_addr.sin_port);
}

const struct sockaddr_in *
InetAddress::get_sockaddr_ptr() const
{
    return &_addr;
}

} /* end of namespace mayu */
