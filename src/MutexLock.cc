#include "../include/MutexLock.hh"

#include "stdio.h"

namespace mayu
{
MutexLock::MutexLock()
{
    int __ret = ::pthread_mutex_init(get_ptr(), nullptr);

    if (__ret) {
        ::perror("pthread_mutex_init");
    }
}

MutexLock::~MutexLock()
{
   int __ret = ::pthread_mutex_destroy(get_ptr());

   if (__ret) {
       ::perror("pthread_mutex_destroy");
   }
}

void
MutexLock::lock()
{
    int __ret = ::pthread_mutex_lock(get_ptr());

    if (__ret) {
        ::perror("pthread_mutex_lock");
    }
}

void MutexLock::unlock()
{
    int __ret = ::pthread_mutex_unlock(get_ptr());

    if (__ret) {
        ::perror("pthread_mutex_unlock");
    }
}

pthread_mutex_t *
MutexLock::get_ptr()
{
    return &_mutex;
}

MutexLockGuard::MutexLockGuard(MutexLock &__mutex)
: _mutex(__mutex)
{
    _mutex.lock();
}

MutexLockGuard::~MutexLockGuard()
{
    _mutex.unlock();
}

} /* end of namespace mayu */
