#include "../include/EchoServer.hh"

#include <iostream>

namespace mayu
{
// 本来应该写在另一个文件的class MyTask
class MyTask
{
public:
    MyTask(const std::string &__msg, const TcpConnectionShptr __con)
    : _msg(__msg)
    , _con(__con)
    {}

    void
    process()
    {
        // 主要业务逻辑
        _con->send_in_loop(_msg + "\n");
    }

private:
    std::string _msg;

    TcpConnectionShptr _con;

};

EchoServer::EchoServer(size_t __thread_num, size_t __taskque_capacity,
                       const std::string &__ip, unsigned short __port)
: _pool(__thread_num, __taskque_capacity)
, _server(__ip, __port)
{}

EchoServer::~EchoServer()
{}

void
EchoServer::start()
{
    _pool.start();

    _server.set_all_callback(std::bind(&EchoServer::_on_connection, this, std::placeholders::_1),
                             std::bind(&EchoServer::_on_message, this, std::placeholders::_1),
                             std::bind(&EchoServer::_on_close, this, std::placeholders::_1));

    _server.start();
}

void
EchoServer::stop()
{
    _server.stop();
    _pool.stop();
}

void
EchoServer::_on_connection(const TcpConnectionShptr &__con)
{
    std::cout << ">> " << __con->to_string() << " has connected!" << std::endl;

    std::string __msg = "Connection built successfully!\n";
    __con->send_in_loop(__msg);
}

void
EchoServer::_on_message(const TcpConnectionShptr &__con)
{
    std::string __msg = __con->recv();
    
    std::cout << "msg = " << __msg << std::endl;

    MyTask __task(__msg, __con);
    _pool.add_task(std::bind(&MyTask::process, __task));

}

void
EchoServer::_on_close(const TcpConnectionShptr &__con)
{
    std::cout << ">> " << __con->to_string() << " has closed!" << std::endl;
}

} /* end of namespace mayu */
